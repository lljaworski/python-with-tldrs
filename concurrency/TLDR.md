[Simple code](simple.py) can have different [characteristics](settings.py) when trying to run it concurrently, 
sometimes changes to the codebase might be required depending on what the [code](test_tasks.py) itself does as well as 
how we decided to make it concurrent.

Threads
-------
- might require changes in [code](threads.py) that are not obvious (synchronization, locking)
- can help only with IO-bound tasks or with libraries that release GIL (+ python implementation that either don't have 
  GIL at all or have option to release it e.g. Cython's `nogil` context manager)
- you can only have so many threads - resources, performance due to too much switching etc

Asyncio
-------------------
- needs changes in the source [code](async.py)
- requires change in thinking - now we need to care when we might hand and should explicitly give the control back
- error prone, took longest to write
- still requires locks, like threading (although potentially not as much)

Processes
---------
- no changes in [code](processes.py) required
- really heavy as you run the same program multiple times
- easy way to scale CPU-bound tasks
- might be left in the memory if the main program crashes and run until killed or finished

Green threads
-------------
- basically async but no changes to the [code](green_threads.py) required
- race conditions are less likely to cause serious errors than in normal threading
- can easily increase performance a couple of times by changing a few lines of [code](patched_threads.py)
- is limited only to libraries it mocks - low level libraries might not be affected at all unlike the model with normal threads
- is an external library which adds stuff to [install](requirements.txt)

Mixing
------
- asyncio can be used combined with processes to handle CPU-bound tasks (scheduler-workers)
- asyncio can be used with threads to handle code not written for asyncio

_People are only as smart as those who surround us, the same way python is as fast as languages we use alongside it._
