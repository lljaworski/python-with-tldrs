# First patch all things that require patching, otherwise something might break.
from gevent import monkey

monkey.patch_all()
# Now the code itself begins.
import gevent

from time import time, monotonic

from concurrency.settings import number_of_tasks, crunch_difficulty, wait_length
from concurrency.test_tasks import wait_for, crunch_numbers

print('=' * 40)
print(f'{monotonic()} -- {time()} -- Starting GREEN THREADS.')
print('=' * 40)
start = time()

green_threads = []

for _ in range(number_of_tasks):
    new_wait_for_task = gevent.spawn(wait_for,
                                     wait_length,
                                     task_number=len(green_threads) + 1)
    green_threads.append(new_wait_for_task)

    new_crunch_numbers_task = gevent.spawn(crunch_numbers,
                                           crunch_difficulty,
                                           task_number=len(green_threads) + 1)
    green_threads.append(new_crunch_numbers_task)

gevent.joinall(green_threads)

end = time()
print('=' * 40)
print(f'Run took {end - start}.')
print('=' * 40)
